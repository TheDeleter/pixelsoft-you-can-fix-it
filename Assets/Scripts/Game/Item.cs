﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour 
{
	//Public
	public ItemType type;
}

//Enum
public enum ItemType
{
	RottenWood,
	Wood
}

public enum Tools
{
	Hammer
}