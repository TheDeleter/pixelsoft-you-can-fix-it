﻿using UnityEngine;
using System.Collections.Generic;

public class Inventory : MonoBehaviour 
{
	//Variables
	bool changeEnabled = false;
	Vector3 hidingPosition;
	Vector3 showingPosition;
	float velocity = 20;
	//Public
	public GameObject inventoryButton;
	public bool inventoryActive = false;
	public GameObject[] toolSlots;
	public SpriteRenderer[] inventorySlots;
	public TextMesh[] QuantitySlots;
	//Patch
	public Sprite rottenWood;
	public Sprite wood;
	public Sprite defaultSprite;

	//Struct
	public class ItemElement
	{
		public ItemType type;
		public int quantity = 0;
	}

	void Start()
	{
		Initialize();
	}

	void Initialize()
	{
		if(Application.loadedLevelName != "Intro")
		{
			hidingPosition = inventoryButton.transform.position + new Vector3(0, 0, 1);
			showingPosition = Camera.main.transform.position + new Vector3(0, 0, 2);
			LoadTools();
		}
	}

	void LateUpdate () 
	{
		if(changeEnabled)
		{
			hidingPosition = inventoryButton.transform.position + new Vector3(0, 0, 1);
			showingPosition = Camera.main.transform.position + new Vector3(0, 0, 2);
			if(inventoryActive)
			{
				transform.position -= new Vector3(0.5f, 0.25f, 0) * Time.deltaTime * velocity;
				transform.localScale += new Vector3(1.6f, 1.6f, 0) * Time.deltaTime * velocity;
				if((transform.position.x < showingPosition.x) && (transform.position.y < showingPosition.y))
				{
					inventoryButton.name = "CloseButton";
					transform.position = showingPosition;
					changeEnabled = false;
					TriggerInventory(true);
				}
			}
			else
			{
				transform.position += new Vector3(0.5f, 0.25f, 0) * Time.deltaTime * velocity;
				transform.localScale -= new Vector3(1.6f, 1.6f, 0) * Time.deltaTime * velocity;
				if((transform.position.x > hidingPosition.x) && (transform.position.y > hidingPosition.y))
				{
					inventoryButton.name = "InventoryButton";
					transform.localScale = new Vector3(3,1.5f,1);
					transform.position = hidingPosition;
					changeEnabled = false;
				}
			}
		}
	}
	
	public void Appear()
	{
		changeEnabled = true;
	}
	
	public void Disappear()
	{
		TriggerInventory(false);
		inventoryActive = false;
		changeEnabled = true;
	}

	public void LoadTools()
	{
		for(int i = 0; i < toolSlots.Length; i++)
		{
			if(!GameManager.instance.Tools[i])
				toolSlots[i].SetActive(false);
			else
				toolSlots[i].SetActive(true);
		}
	}

	void TriggerInventory(bool type)
	{
		if(type)
		{
			for(int i =0; i < GameManager.instance.GameItems.Count; i++)
			{
				QuantitySlots[i].text = GameManager.instance.GameItems[i].quantity.ToString();
				switch(GameManager.instance.GameItems[i].type)
				{
				case ItemType.RottenWood:
					inventorySlots[i].sprite = rottenWood;
					break;
				case ItemType.Wood:
					inventorySlots[i].sprite = wood;
					break;
				}
			}
			foreach(Transform t in this.gameObject.transform)
			{
				t.gameObject.GetComponent<Fade>().Type("In");
			}
		}
		else
		{
			for(int i =0; i < GameManager.instance.GameItems.Count; i++)
			{
				QuantitySlots[i].text = "";
			}
			foreach(Transform t in this.gameObject.transform)
			{
				t.gameObject.GetComponent<Fade>().Type("Out");
			}
		}
	}

	public void AddItem(ItemType itemType, int quantity)
	{
		ItemElement temp;
		bool newItem = false;
		if(GameManager.instance.GameItems.Count == 0)
		{
			GameManager.instance.GameItems.Add(AddNew(itemType,quantity));
		}
		else
		{

			for(int i = 0; i < GameManager.instance.GameItems.Count; i++)
			{
				if(GameManager.instance.GameItems[i].type == itemType)
				{
					GameManager.instance.GameItems[i].quantity++;
					newItem = false;
				}
				else
				{
					newItem = true;
				}
			}
			if(newItem)
			{
				GameManager.instance.GameItems.Add(AddNew(itemType,quantity));
			}

		}
	}

	ItemElement AddNew(ItemType itemType,int quantity)
	{
		ItemElement temp = new ItemElement();
		temp.type = itemType;
		temp.quantity = quantity;
		return temp;
	}
}
