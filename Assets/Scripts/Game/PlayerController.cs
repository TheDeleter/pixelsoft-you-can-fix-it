using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	public Vector3 newPosition;
	float velocity;
	int direction = 0;
	Animator animation;

	public bool isMoving = false;
	public bool isExiting = false;

	void Start()
	{
		velocity = GameManager.instance.playerVelocity;
		animation = this.gameObject.GetComponent<Animator>();

		if(GameManager.instance.previousScenePosition != null)
		{
			if(GameManager.instance.previousLevel == "Lobby")
			{
				gameObject.transform.position = GameManager.instance.previousScenePosition;
				Camera.main.gameObject.transform.position = new Vector3(5,Camera.main.gameObject.transform.position.y,Camera.main.gameObject.transform.position.z);
			}
		}
		ChoosePlayerOrientation();
	}

	void Update ()
	{
		//Movement
		switch(direction)
		{
		case 0:	isExiting = false; isMoving = false; break;
		case 1:	transform.position += new Vector3(velocity * Time.deltaTime,0,0);
				transform.eulerAngles = new Vector3(0,180,0);
				if(transform.position.x > newPosition.x) direction = 0;	break;
		case 2:	transform.position -= new Vector3(velocity * Time.deltaTime,0,0);
				transform.eulerAngles = new Vector3(0,0,0);
				if(transform.position.x < newPosition.x) direction = 0;	break;
		}
		//Animation
		animation.SetBool("Walking", isMoving);
	}

	void OnTriggerStay2D (Collider2D other)
	{
		if(isExiting)
		{
			GameManager.instance.blackScreen.Type("In");
		}
		if(other.gameObject.name.Contains("Checkpoint"))
		{
			GameManager.instance.CheckpointTrigger(int.Parse(other.gameObject.name.Substring(11)));
			other.gameObject.collider2D.enabled = false;
		}
		else if(other.gameObject.name == "Recycle Machine")
		{
			for(int i = 0; i < GameManager.instance.GameItems.Count; i++)
			{
				switch(GameManager.instance.GameItems[i].type)
				{
				case ItemType.RottenWood:
					GameManager.instance.inventory.AddItem(ItemType.Wood,GameManager.instance.GameItems[i].quantity);
					GameManager.instance.GameItems.RemoveAt(i);
					break;
				}
			}
		}
		else if(other.gameObject.name.Contains("Unfixed"))
		{
			other.gameObject.name = "Lobby Door";
			other.gameObject.GetComponent<SpriteRenderer>().sprite = GameManager.instance.fixedDoor;
			GameManager.instance.GameItems.Clear();
		}
	}

	void OnCollisionEnter2D (Collision2D other)
	{
		if(other.gameObject.name == "Item")
		{
			GameManager.instance.inventory.AddItem(other.gameObject.GetComponent<Item>().type, 1);
			Destroy(other.gameObject);
		}
		else if (other.gameObject.name.Contains("Tool"))
		{
			int toolAquired = int.Parse(other.gameObject.name.Substring(5,1));
			GameManager.instance.Tools[toolAquired] = true;
			GameManager.instance.inventory.LoadTools();
			Destroy(other.gameObject);
		}
	}

	public void Move(Vector3 input)
	{
		newPosition = input;

		if(input.x > transform.position.x)
			direction = 1;
		else
			direction = 2;
	}

	void ChoosePlayerOrientation()
	{
		CameraFollowPlayer cam = Camera.main.gameObject.GetComponent<CameraFollowPlayer>();
		float middlePosition = (cam.cameraLimitLeft.position.x + cam.cameraLimitRight.position.x) / 2;

		if(transform.position.x < middlePosition)
			transform.eulerAngles = new Vector3(0,180,0);
	}
}

