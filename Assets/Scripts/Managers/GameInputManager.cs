﻿using UnityEngine;
using System.Collections;

public class GameInputManager : MonoBehaviour 
{
	bool onMobile;
	public Vector3 currentPosition;

	void Start ()
	{
		#if UNITY_EDITOR
		onMobile = false;
		#elif UNITY_STANDALONE
		onMobile = false;
		#elif UNITY_WEBPLAYER
		onMobile = false;
		#else
		onMobile = true;
		#endif
	}
	
	void Update ()
	{
		if(onMobile)
			DetectTouch();
		else
			DetectMouse();
	}

	void DetectTouch()
	{
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
			DetectObject(GetObjectInPosition(Input.GetTouch(0).position));

		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
		{
			if(ValidateCoordinates(Input.GetTouch(0).position))
			{
				MovePlayer();
			}
		}
	}
	
	void DetectMouse()
	{
		if(Input.GetMouseButtonDown(0))
			DetectObject(GetObjectInPosition(Input.mousePosition));

		if(Input.GetMouseButton(0))
		{
			if(ValidateCoordinates(Input.mousePosition))
			{
				MovePlayer();
			}
		}
	}

	GameObject GetObjectInPosition(Vector3 curScreenPoint)
	{		
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(curScreenPoint), Vector2.zero);
		if((hit.collider != null))
		{
			if(hit.collider.gameObject.name.Contains("Button"))
				GameManager.instance.inventory.inventoryActive = true;
			return (hit.collider.gameObject);
		}
		else
			return null;
	}

	void MovePlayer()
	{
		GameManager.instance.player.Move(Camera.main.ScreenToWorldPoint(currentPosition));
		GameManager.instance.player.isMoving = true;
	}

	bool ValidateCoordinates(Vector3 position)
	{
		if(GameManager.instance.inventory.inventoryActive)
			return false;
		if(position.x < GameManager.instance.screenLimits.x)
			return false;
		if(position.x > GameManager.instance.screenLimits.y)
			return false;
		if(position.y < GameManager.instance.screenLimits.z)
			return false;
		if(position.y > GameManager.instance.screenLimits.w)
			return false;
		if(Mathf.Abs(Camera.main.ScreenToWorldPoint(position).x - GameManager.instance.player.gameObject.transform.position.x) < 0.1f)
			return false;

		currentPosition = position;

		return true;
	}

	void DetectObject(GameObject other)
	{
		if(other == null)
			return;

		if(!other.name.Contains("Unfixed"))
		{
			if(other.name.Contains("Door"))
			{
				GameManager.instance.player.isExiting = true;
				GameManager.instance.nextLevel = other.name.Split(' ')[0];
			}
		}
	}
}
