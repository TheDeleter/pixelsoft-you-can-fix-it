﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour
{
	bool onMobile;
	GameObject gameObjectClicked;
	
	void Start ()
	{
		#if UNITY_EDITOR
		onMobile = false;
		#elif UNITY_STANDALONE
		onMobile = false;
		#elif UNITY_WEBPLAYER
		onMobile = false;
		#else
		onMobile = true;
		#endif		
	}
	
	void Update ()
	{
		if(onMobile)
		{
			DetectTouch();
		}
		else
		{
			DetectMouse();
		}
	}
	
	void DetectMouse()
	{
		if (Input.GetMouseButtonDown(0))
		{
			gameObjectClicked = GetObjectInPosition(Input.mousePosition);
			if(gameObjectClicked != null)
			{
				MouseDown();
			}
		}
		
		if(gameObjectClicked != null)
		{
			if (Input.GetMouseButton(0))
			{
				if (GetObjectInPosition(Input.mousePosition) != gameObjectClicked)
				{
					MouseOut();
					gameObjectClicked = null;
				}
			}
			
			if (Input.GetMouseButtonUp(0))
			{
				ClickedObject();
				gameObjectClicked = null;
			}
		}
	}
	
	void DetectTouch()
	{
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
		{
			gameObjectClicked = GetObjectInPosition(Input.GetTouch(0).position);
			if(gameObjectClicked != null)
			{
				MouseDown();
			}
		}
		if(gameObjectClicked != null)
		{
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
			{
				if (GetObjectInPosition(Input.GetTouch(0).position) != gameObjectClicked)
				{
					MouseOut();
					gameObjectClicked = null;
				}
			}
			
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
			{
				ClickedObject();
				gameObjectClicked = null;
			}
		}
	}

	GameObject GetObjectInPosition(Vector3 curScreenPoint)
	{		
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(curScreenPoint), Vector2.zero);
		if(hit.collider != null)
			return (hit.collider.gameObject);
		else
			return null;
	}

	void MouseDown()
	{
		gameObjectClicked.SendMessage("MouseDown",SendMessageOptions.DontRequireReceiver);
	}
	
	void MouseOut()
	{
		gameObjectClicked.SendMessage("MouseOut",SendMessageOptions.DontRequireReceiver);
	}
	
	void ClickedObject()
	{
		gameObjectClicked.SendMessage("Clicked",SendMessageOptions.DontRequireReceiver);
	}
}
