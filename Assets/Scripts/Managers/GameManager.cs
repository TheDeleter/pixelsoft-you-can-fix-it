﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
	//Instances
	public static GameManager instance;
	//InputManagers
	public GameInputManager gameInputManager;
	//Game Variables
	public Vector4 screenLimits;
	public string nextLevel;
	public float playerVelocity;
	public PlayerController player;
	public Fade blackScreen;
	public Inventory inventory;
	//Checkpoints
	public string checkpoints = "00000";
	//Inventory
	public List<Inventory.ItemElement> GameItems;
	public bool[] Tools;
	//Temporal Patches
	public Sprite fixedDoor;
	public Vector3 previousScenePosition;
	public string previousLevel;
	public bool grabbedItems = false;
	public Transform itemsGrabbed;

	void Start()
	{
		//Instance
		if(instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
			Initialize();
		}
		else
		{
			LoadGameObjects();
			DestroyImmediate(this.gameObject);
		}
	}

	void Initialize()
	{
		//Calculate screen size
		screenLimits.x = Screen.width  * 0.05f;
		screenLimits.y = Screen.width  * 0.95f;
		screenLimits.z = Screen.height * 0.05f;
		screenLimits.w = Screen.height * 0.95f;
		//PlayerPrefs
		checkpoints = PlayerPrefs.GetString("Checkpoints", checkpoints);
		//Initialize Objects
		GameItems = new List<Inventory.ItemElement>();
	}

	void LoadGameObjects()
	{
		if(Application.loadedLevelName != "Intro")
		{
			//Find objects
			instance.player = GameObject.Find("Player").GetComponent<PlayerController>();
			instance.blackScreen = GameObject.Find("BlackScreen").GetComponent<Fade>();
			instance.gameInputManager = GameObject.Find("Input Manager").GetComponent<GameInputManager>();
			instance.inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
			//Patch
			instance.itemsGrabbed = GameObject.Find("Items").transform;
			if(instance.grabbedItems)
			{
				foreach(Transform child in instance.itemsGrabbed)
				{
					Destroy(child.gameObject);
				}
				if(Application.loadedLevelName == "Tutorial")
				{
					GameObject.Find("Unfixed Door").GetComponent<SpriteRenderer>().sprite = fixedDoor;
					GameObject.Find("Unfixed Door").name = "Lobby Door";
				}
			}
		}
	}

	void SaveGame()
	{
		//Checkpoints
		//PlayerPrefs.SetString("Checkpoints", checkpoints);
	}

	public void ChangeLevel()
	{
		grabbedItems = true;
		previousLevel = Application.loadedLevelName;
		if(Application.loadedLevelName == "Tutorial")
			previousScenePosition = player.gameObject.transform.position;
		//Save Game
		SaveGame();
		//Load Level
		Application.LoadLevel(nextLevel);
	}

	public void CheckpointTrigger(int index)
	{
		if(checkpoints[index] == '1')
			return;
		//Trigger message and stuff
		checkpoints = checkpoints.Substring(0,index) + "1" + checkpoints.Substring(index + 1);
	}
}
