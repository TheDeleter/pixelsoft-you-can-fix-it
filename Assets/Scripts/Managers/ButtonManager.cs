﻿using UnityEngine;
using System.Collections;

public class ButtonManager : MonoBehaviour
{
	Color normalColor = new Color(1f,1f,1f);
	Color pressedColor = new Color(0.5f,0.5f,0.5f);

	SpriteRenderer sr;

	CameraController cameraController;

	Sprite normalSprite;
	public Sprite pressSprite;

	public GameObject[] obj;

	void Awake () 
	{
		sr = GetComponent<SpriteRenderer>();

		if(this.pressSprite != null)
		{
			normalSprite = sr.sprite;
		}
	}

	void Start()
	{
		cameraController = Camera.main.GetComponent<CameraController>();
	}

	void MouseDown()
	{
		if(pressSprite == null)
			this.sr.color = pressedColor;
		else
			sr.sprite = pressSprite;
	}
	
	void MouseOut()
	{
		if(pressSprite == null)
			this.sr.color = normalColor;
		else
			sr.sprite = normalSprite;
	}
	
	void Clicked()
	{
		//Buttons
		if(Application.loadedLevelName == "Intro")
		{
			//In Menu Behaviour
			if(this.name == "IntroButton")
				cameraController.GoToDesiredScene(MenuScenes.INTRO);
			else if(this.name == "GameButton")
				cameraController.GoToDesiredScene(MenuScenes.GAME);
			else if(this.name == "SettingsButton")
				cameraController.GoToDesiredScene(MenuScenes.SETTINGS);
			//To Another Scenes
			else if(this.name == "StartGame")
				Application.LoadLevel("Tutorial");
		}
		else
		{
			//Game Buttons
			if(this.name == "InventoryButton")
				GameManager.instance.inventory.Appear();
			else if(this.name == "CloseButton")
				GameManager.instance.inventory.Disappear();
		}

		if(pressSprite == null)
			this.sr.color = normalColor;
		else
			sr.sprite = normalSprite;

	}
}
