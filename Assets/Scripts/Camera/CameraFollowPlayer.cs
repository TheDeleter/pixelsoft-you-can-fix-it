﻿using UnityEngine;
using System.Collections;

public class CameraFollowPlayer : MonoBehaviour 
{
	public 	Transform cameraLimitRight;
	public 	Transform cameraLimitLeft;
	private Vector2 playerLimits;
	float velocity;

	void Start()
	{
		CalculatePlayerLimits();
		velocity = GameManager.instance.playerVelocity;
	}

	void LateUpdate () 
	{
		float playerPosition = GameManager.instance.player.gameObject.transform.position.x;
		
		if(Camera.main.transform.position.x < cameraLimitRight.position.x)
		{
			if(playerPosition > playerLimits.y)
			{
				Camera.main.transform.position += Vector3.right * velocity * Time.deltaTime;
				CalculatePlayerLimits();
			}
		}
		if(Camera.main.transform.position.x > cameraLimitLeft.position.x)
		{
			if(playerPosition < playerLimits.x)
			{
				Camera.main.transform.position -= Vector3.right * velocity * Time.deltaTime;
				CalculatePlayerLimits();
			}
		}
	}

	void CalculatePlayerLimits()
	{
		playerLimits.x = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.30f, 0, 0)).x;
		playerLimits.y = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.70f, 0, 0)).x;
	}
}
