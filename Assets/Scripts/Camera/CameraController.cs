﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
	Vector3 desiredPosition;
	public int originalSpeed;
	public Fade blackScreen;
	float speed;

	Vector3 Intro 		= new Vector3(   0, 0, -10);
	Vector3 Game	 	= new Vector3( -15, 0, -10);
	Vector3 Settings 	= new Vector3(  15, 0, -10);

	void Start()
	{
		speed = originalSpeed;
		desiredPosition = Intro;
		//Black Screen
		blackScreen.Type("Out");;
	}

	void LateUpdate()
	{
		if(desiredPosition == this.transform.position)
			return;

		if(Vector3.Distance(desiredPosition, this.transform.position) < .5f)
		{
			speed = originalSpeed * 1.5f;
		}

		if(Vector3.Distance(desiredPosition, this.transform.position) < 0.01f)
		{
			this.transform.position = desiredPosition;
			speed = originalSpeed;
		}
		else
		{
			Vector3 movement = desiredPosition - this.transform.position;
			this.transform.position += movement * speed * Time.deltaTime;
		}
	}

	public void GoToDesiredScene(MenuScenes ChosenScene)
	{
		switch(ChosenScene)
		{
		case MenuScenes.INTRO: 			desiredPosition = Intro; break;
		case MenuScenes.SETTINGS: 		desiredPosition = Settings; break;
		case MenuScenes.GAME:			desiredPosition = Game; break;
		}
	}
}

public enum MenuScenes
{
	INTRO,
	SETTINGS,
	GAME
}
