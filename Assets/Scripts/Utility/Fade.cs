﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour {

	SpriteRenderer FadeSprite;
	string fadeType;
	bool start = false;
	Color originalColor;

	float tInterval = 0.01f;
	float tCounter;

	public float factor;

	void Start()
	{
		FadeSprite = this.gameObject.GetComponent<SpriteRenderer>();
		originalColor = FadeSprite.color;
		ActionStart();
	}

	void Awake()
	{
		tCounter = Time.time + tInterval;
	}


	void Update () 
	{
		if(!start)
			return;

		if(fadeType == "In")
		{
			if(tCounter < Time.time)
			{
				tCounter = Time.time + tInterval;
				FadeSprite.color += new Color(0,0,0,factor);
			}
			if(FadeSprite.color.a >= 1)
			{
				start = false;
				ActionIn();
			}
		}
		else
		{
			if(tCounter < Time.time)
			{
				tCounter = Time.time + tInterval;
				FadeSprite.color -= new Color(0,0,0,factor);
			}
			if(FadeSprite.color.a <= 0)
			{
				start = false;
				ActionOut();
			}
		}
		return;
	}
	
	void ActionStart()
	{
		if(this.gameObject.name == "BlackScreen")
			Type("Out");
	}

	void ActionStartIn()
	{
		if(this.gameObject.name == "BlackScreen")
			GameManager.instance.player.isExiting = false; 
	}

	void ActionStartOut()
	{

	}

	void ActionIn()
	{
		if(this.gameObject.name == "BlackScreen")
			GameManager.instance.ChangeLevel();
	}

	void ActionOut()
	{

	}

	public void Type(string type)
	{
		start = true;
		fadeType = type;
		if(type == "In")
		{
			ActionStartIn();
			FadeSprite.color = originalColor - new Color(0,0,0,1);
		}
		else
		{
			ActionStartOut();
		}
	}
}
